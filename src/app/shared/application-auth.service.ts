import {Injectable} from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFirestore} from "@angular/fire/firestore";
import {User} from "firebase";
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApplicationAuthService {
  public readonly user: Subject<UserProfile> = new Subject();

  constructor(public readonly firebaseAuth: AngularFireAuth,
              private db: AngularFirestore) {
    this.firebaseAuth.user.subscribe(user => {
      if (!user) {
        console.log("No user!");
        return;
      }
      this.db.collection<UserAdditionalInfo>("users")
        .doc(user.uid)
        .get()
        .subscribe(userInfo => {
          if (userInfo && userInfo.exists) {
            this.populateUser(user, userInfo);
          }
        });
    })
  }

  private populateUser(user, userInfo) {
    this.user.next({
      user: user,
      additionalInfo: userInfo.data()
    });
  }
}

export interface UserAdditionalInfo {
  name: string;
  role: string,
  uid: string
}

export interface UserProfile {
  user: User;
  additionalInfo: UserAdditionalInfo;
}
