import { Component, OnInit } from '@angular/core';
import {HotelListItem} from '../hotel-list-page/hotels-list/shared/hotel-list-item';

@Component({
  selector: 'app-hotel-list-page',
  templateUrl: './hotel-list-page.component.html',
  styleUrls: ['./hotel-list-page.component.less']
})
export class HotelListPageComponent {
  title = 'ngHotelsList';
  hotelDetail: HotelListItem;

  changeHotelDetail(hotel: HotelListItem) {
    this.hotelDetail = hotel;
  }
}
