import {Component} from '@angular/core';
import {ApplicationAuthService} from "./shared/application-auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  login: String;


  constructor(private authService: ApplicationAuthService) {
    authService.user.subscribe(user=>this.login = user.additionalInfo.name + " " + user.additionalInfo.role)
  }
}
