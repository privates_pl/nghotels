import {Injectable} from '@angular/core';
import {HotelListItem} from './hotel-list-item';
import {Observable, of} from 'rxjs';
import {AngularFirestore, DocumentReference} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private db: AngularFirestore) {
  }

  public getHotels(): Observable<HotelListItem[]> {
    return this.db.collection<HotelListItem>('hotels').valueChanges();
  }

  addHotel(hotelListItem: HotelListItem): Promise<DocumentReference> {
    return this.db.collection<HotelListItem>('hotels').add({name: hotelListItem.name});
  }
}
