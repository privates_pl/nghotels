import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewHotelDetailComponent } from './new-hotel-detail.component';

describe('NewHotelDetailComponent', () => {
  let component: NewHotelDetailComponent;
  let fixture: ComponentFixture<NewHotelDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewHotelDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHotelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
