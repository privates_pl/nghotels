import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelsListFilterComponent } from './hotels-list-filter.component';

describe('HotelsListFilterComponent', () => {
  let component: HotelsListFilterComponent;
  let fixture: ComponentFixture<HotelsListFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelsListFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelsListFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
