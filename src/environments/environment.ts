// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC1dzbAVB_4s__MBwk_TF61990yjGaHDZA',
    authDomain: 'nghotels-79c76.firebaseapp.com',
    databaseURL: 'https://nghotels-79c76.firebaseio.com',
    projectId: 'nghotels-79c76',
    storageBucket: 'nghotels-79c76.appspot.com',
    messagingSenderId: '101309984887',
    appId: '1:101309984887:web:cf20d3729a5e266ec09672',
    measurementId: 'G-XYZWZ1T8NZ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
