import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewHotelsListItemComponent } from './new-hotels-list-item.component';

describe('NewHotelsListItemComponent', () => {
  let component: NewHotelsListItemComponent;
  let fixture: ComponentFixture<NewHotelsListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewHotelsListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHotelsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
