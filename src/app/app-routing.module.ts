import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {LoginPageComponent} from './login-page/login-page-component';
import {HotelListPageComponent} from './hotel-list-page/hotel-list-page.component';
import {map} from "rxjs/operators";

const redirectUnauthorizedToLogin = () => map(user => !user ? ['login'] : true );

const routes: Routes = [
  {
    path: 'hotels', component: HotelListPageComponent
    // canActivate: [AngularFireAuthGuard],
    // data: { authGuardPipe: redirectUnauthorizedToLogin }
  },
  {path: 'login', component: LoginPageComponent},
  {path: '**', redirectTo: 'hotels', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
