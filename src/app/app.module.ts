import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HotelsListComponent} from './hotel-list-page/hotels-list/hotels-list.component';
import {HotelsListFilterComponent} from './hotel-list-page/hotels-list/hotels-list-filter/hotels-list-filter.component';
import {HotelsListItemComponent} from './hotel-list-page/hotels-list/hotels-list-item/hotels-list-item.component';
import {HotelDetailComponent} from './hotel-list-page/hotel-detail/hotel-detail.component';
import {NewHotelDetailComponent} from './hotel-list-page/new-hotel-list/new-hotel-detail/new-hotel-detail.component';
import {NewHotelListComponent} from './hotel-list-page/new-hotel-list/new-hotel-list.component';
import {NewHotelsListItemComponent} from './hotel-list-page/new-hotel-list/new-hotels-list-item/new-hotels-list-item.component';
import {HorizontalSeparatorComponent} from './shared/horizontal-separator/horizontal-separator.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {NewHotelComponent} from './hotel-list-page/new-hotel/new-hotel.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginPageComponent} from './login-page/login-page-component';
import {AngularFireAuthGuard} from '@angular/fire/auth-guard';
import { HotelListPageComponent } from './hotel-list-page/hotel-list-page.component';
import {AngularFireAuthModule} from '@angular/fire/auth';

@NgModule({
  declarations: [
    AppComponent,
    HotelsListComponent,
    HotelsListFilterComponent,
    HotelsListItemComponent,
    HotelDetailComponent,
    NewHotelDetailComponent,
    NewHotelListComponent,
    NewHotelsListItemComponent,
    HorizontalSeparatorComponent,
    NewHotelComponent,
    LoginPageComponent,
    HotelListPageComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [
    AngularFireAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
