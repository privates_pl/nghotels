import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewHotelListComponent } from './new-hotel-list.component';

describe('NewHotelListComponent', () => {
  let component: NewHotelListComponent;
  let fixture: ComponentFixture<NewHotelListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewHotelListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHotelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
