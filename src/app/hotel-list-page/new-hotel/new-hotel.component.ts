import {Component, OnInit} from '@angular/core';
import {HotelService} from '../hotels-list/shared/hotel.service';
import {HotelListItem} from '../hotels-list/shared/hotel-list-item';

@Component({
  selector: 'app-new-hotel',
  templateUrl: './new-hotel.component.html',
  styleUrls: ['./new-hotel.component.less']
})
export class NewHotelComponent implements OnInit {
  name = '';

  constructor(private hotelService: HotelService) {
  }

  ngOnInit() {
  }

  addHotel() {
    this.hotelService.addHotel(new HotelListItem(this.name));
  }
}
