import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApplicationAuthService} from "../shared/application-auth.service";

@Component({
  selector: 'app-login-page-component',
  templateUrl: './login-page-component.html',
  styleUrls: ['./login-page-component.less']
})
export class LoginPageComponent implements OnInit {
  login: string;
  password: string;

  constructor(private fireAuth: ApplicationAuthService, private  router: Router) {
  }

  ngOnInit() {
  }

  doLogin(): void {

    console.log(this.login, this.password);
    this.fireAuth.firebaseAuth.auth.signInWithEmailAndPassword(this.login, this.password).catch((error) => {
      console.log(error.code, error.message);
    }).then((message: any) => {
      console.log('logged as ', message.user.email);

      this.router.navigate(['hotels']);
    });
  }

}
