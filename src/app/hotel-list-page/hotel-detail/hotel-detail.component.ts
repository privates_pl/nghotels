import {Component, OnInit} from '@angular/core';
import {HotelDetailService} from '../hotels-list/shared/hotel-detail.service';
import {HotelListItem} from '../hotels-list/shared/hotel-list-item';

@Component({
  selector: 'app-hotel-detail',
  templateUrl: './hotel-detail.component.html',
  styleUrls: ['./hotel-detail.component.less']
})
export class HotelDetailComponent implements OnInit {
  public hotel: HotelListItem;

  constructor(private hotelDetailService: HotelDetailService) {
    hotelDetailService.showHotelDetailObservable.subscribe(hotel => this.onHotelChanged(hotel));
  }

  ngOnInit() {
  }

  private onHotelChanged(hotel: HotelListItem) {
    this.hotel = hotel;
  }

}
