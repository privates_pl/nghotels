import {Component, EventEmitter, Output} from '@angular/core';
import {HotelListItem} from '../hotels-list/shared/hotel-list-item';
import {HotelService} from '../hotels-list/shared/hotel.service';
import {FilterData} from '../hotels-list/shared/filter-data';

@Component({
  selector: 'app-new-hotel-list',
  templateUrl: './new-hotel-list.component.html',
  styleUrls: ['./new-hotel-list.component.less']
})
export class NewHotelListComponent {
  items: HotelListItem[] = [];
  originalItems: HotelListItem[];
  filter = new FilterData();

  @Output()
  hotelChange = new EventEmitter<HotelListItem>();

  constructor(private hotelService: HotelService) {
    hotelService.getHotels().subscribe(hotels => {
        this.originalItems = hotels;
        this.updateItems();
      });
  }

  private updateItems(originalItems = this.originalItems) {
    this.items = originalItems.filter(item =>
      this.filter.isEmpty() ||
      item.name.toLowerCase().includes(this.filter.name.toLowerCase()));
  }

  public filterChanged(filter: FilterData) {
    this.filter = filter;
    this.updateItems();
  }

  public showHotelDetail(hotel: HotelListItem) {
    this.hotelChange.emit(hotel);
  }
}
