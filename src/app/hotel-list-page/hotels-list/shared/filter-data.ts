export class FilterData {
  name: string;

  isEmpty(): boolean {
    return this.name == null || this.name.length === 0;
  }
}
