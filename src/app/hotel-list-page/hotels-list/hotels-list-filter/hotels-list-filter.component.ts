import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FilterData} from '../shared/filter-data';

@Component({
  selector: 'app-hotels-list-filter',
  templateUrl: './hotels-list-filter.component.html',
  styleUrls: ['./hotels-list-filter.component.less']
})
export class HotelsListFilterComponent implements OnInit {
  name: string;
  data: FilterData;

  @Output()
  filterChanged = new EventEmitter<FilterData>();

  constructor() {
    this.data = new FilterData();
  }

  ngOnInit() {
  }


  changeName(newValue: string) {
    this.data.name = newValue;
    this.filterChanged.emit(this.data);
  }

}
