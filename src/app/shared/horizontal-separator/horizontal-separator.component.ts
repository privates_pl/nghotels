import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-horizontal-separator',
  templateUrl: './horizontal-separator.component.html',
  styleUrls: ['./horizontal-separator.component.less']
})
export class HorizontalSeparatorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
