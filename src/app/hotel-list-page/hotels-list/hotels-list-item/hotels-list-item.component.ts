import {Component, Input, OnInit} from '@angular/core';
import {HotelListItem} from '../shared/hotel-list-item';
import {HotelDetailService} from '../shared/hotel-detail.service';

@Component({
  selector: 'app-hotels-list-item',
  templateUrl: './hotels-list-item.component.html',
  styleUrls: ['./hotels-list-item.component.less']
})
export class HotelsListItemComponent implements OnInit {

  @Input()
  data: HotelListItem;

  constructor(private hotelDetailService: HotelDetailService) {
  }

  ngOnInit() {
  }

  public showDetails() {
    this.hotelDetailService.showHotelDetail(this.data);
  }

}
