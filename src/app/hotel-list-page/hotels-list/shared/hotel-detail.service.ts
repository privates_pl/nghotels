import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {HotelListItem} from './hotel-list-item';

@Injectable({
  providedIn: 'root'
})
export class HotelDetailService {

  constructor() { }

  // Observable sources
  private showHotelDetailSource = new Subject<HotelListItem>();

  // Observable streams
  showHotelDetailObservable = this.showHotelDetailSource.asObservable();

  // Service message commands
  showHotelDetail(hotel: HotelListItem) {
    this.showHotelDetailSource.next(hotel);
  }
}
