import {Component, Input, OnInit} from '@angular/core';
import {HotelListItem} from '../../hotels-list/shared/hotel-list-item';

@Component({
  selector: 'app-new-hotel-detail',
  templateUrl: './new-hotel-detail.component.html',
  styleUrls: ['./new-hotel-detail.component.less']
})
export class NewHotelDetailComponent implements OnInit {
  @Input()
  hotel: HotelListItem;

  constructor() {
  }

  ngOnInit() {
  }


}
