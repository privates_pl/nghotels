import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HotelListItem} from '../../hotels-list/shared/hotel-list-item';

@Component({
  selector: 'app-new-hotels-list-item',
  templateUrl: './new-hotels-list-item.component.html',
  styleUrls: ['./new-hotels-list-item.component.less']
})
export class NewHotelsListItemComponent implements OnInit {

  @Input()
  data: HotelListItem;
  @Output()
  showDetailsEvent = new EventEmitter<HotelListItem>()

  constructor() {
  }

  ngOnInit() {
  }

  public showDetails() {
    this.showDetailsEvent.emit(this.data);
  }

}
