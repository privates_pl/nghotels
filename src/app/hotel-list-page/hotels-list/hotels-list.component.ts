import {Component, OnInit} from '@angular/core';
import {HotelListItem} from './shared/hotel-list-item';
import {FilterData} from './shared/filter-data';
import {HotelService} from './shared/hotel.service';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-hotels-list',
  templateUrl: './hotels-list.component.html',
  styleUrls: ['./hotels-list.component.less']
})
export class HotelsListComponent {
  items: Observable<Array<HotelListItem>>;
  originalItems: Observable<Array<HotelListItem>>;

  constructor(private hotelService: HotelService) {
    this.originalItems = hotelService.getHotels();
    this.items = this.originalItems;
  }

  public filterChanged(data: FilterData) {
    this.items = this.originalItems.pipe(
      map(hotels => hotels.filter(item =>
        item.name.toLowerCase().includes(data.name.toLowerCase())))
    );
  }

}
